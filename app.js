
/**
 * Module dependencies.
 */

var express = require('express')
  , root = require('./routes/index')
  , pianobar = require('./routes/pianobar')

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');  
  
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('hello'));
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
  //app.enable("jsonp callback");
});

GLOBAL.musicDir='/mnt/Music/Pandora/';
GLOBAL.pianobarStatusDir='/tmp/pianobar/';
GLOBAL.nowPlaying=GLOBAL.pianobarStatusDir + 'nowplaying';
GLOBAL.controlFile=GLOBAL.pianobarStatusDir + 'ctl';
GLOBAL.stationList=GLOBAL.pianobarStatusDir + 'station_list';

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', root.index);
app.get('/pianobar/status/:stationname?', pianobar.getStatus);
app.get('/pianobar/action/:id/:param?', pianobar.action);

app.listen(2000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
