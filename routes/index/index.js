
/*
 * GET home page.
 */

exports.index = function(req, res){
  var currentMode = 'RealTimePlayerMode';

  res.render('index', { title: 'PianoBar WebUI' , 
        locals: {
            currentMode: currentMode,
        }});
};