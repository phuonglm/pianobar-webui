
/*
 * GET home page.
 */

exports.getStatus = function(req, res){
  var fs = require("fs");
  var stations = new Array();
  var currentSong = new Object();
  try{
      var currentSongLine = fs.readFileSync(GLOBAL.nowPlaying).toString().split('\n')[0].split('##');
      currentSong.title = currentSongLine[0].trim();
      currentSong.artist = currentSongLine[1].trim();
      currentSong.album = currentSongLine[2].trim();
      currentSong.stationName = currentSongLine[3].trim();
      
      var stationsLine = fs.readFileSync(GLOBAL.stationList).toString().split('\n');
      stationsLine.pop();
      
      for(var index in stationsLine) {
        var lineItems = stationsLine[index].split('##');
        var selected = false;
        var stationName =  lineItems[1].trim();
        if ( stationName == currentSong.stationName){
            selected = true;
        }
        var station = { id: lineItems[0].trim(), title: stationName, selected: selected};
        stations.push(station);
      }
      var musicsRecorded = new Array();
      if(typeof req.params.stationname == "undefined" || req.params.stationname == "All"){
      } else {
        
        var fileList = fs.readdirSync(GLOBAL.musicDir + req.params.stationname);
        for( var fileListIndex in fileList ){
            var filename = fileList[fileListIndex];
            if( filename.lastIndexOf('.',0) != 0 && filename.indexOf('.aac') === filename.length - 4) {
                var filenameData = filename.substring(0, filename.length - 4).split("---");;
                var musicRecoredItem = {artist: filenameData[0], album: filenameData[1], title: filenameData[2]}; 
                musicsRecorded.push(musicRecoredItem);
            }
        }
      }
      
  } catch(e) {
      console.log(e);
  }
  var jsonData = {currentSong: currentSong, stations: stations, recordedSongs: musicsRecorded};
  res.json(jsonData);
};

exports.action = function(req, res){
    var sys = require('sys')
    var exec = require('child_process').exec;
    var action = { id: req.params.id, param: req.params.param};
    var rejustJson = {};
    function puts(error, stdout, stderr) { sys.puts(stdout) }
    switch (action.id) {
        case "selectStation":
            exec("echo s" + action.param + " > " + GLOBAL.controlFile, puts);
            break;
        case "stop":
            exec("echo -n o > " + GLOBAL.controlFile, puts);
            break;            
        case "playPause":
            exec("echo -n p > " + GLOBAL.controlFile, puts);
            break;
        case "next":
            exec("echo -n n > " + GLOBAL.controlFile, puts);
            break;
        case "like":
            exec("echo -n + > " + GLOBAL.controlFile, puts);
            break;
        case "unlike":
            exec("echo -n - > " + GLOBAL.controlFile, puts);
            break;
    }
    res.json(action);
};