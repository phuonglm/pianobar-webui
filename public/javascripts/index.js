var preivousStatusData = {};
var currentRecordedStation = "All";

function jsonCompare(o1, o2){
  return JSON.stringify(o1) !== JSON.stringify(o2);
}

function playRecord(event){
    var imageLink = event.target['src'];
    var mediaLink = imageLink.substring(0, imageLink.length - 4) + ".aac";
    $("#Stop").trigger('click');    
    $("#jquery_jplayer").jPlayer("clearMedia");
    $("#jquery_jplayer").jPlayer("setMedia", {
        m4a: mediaLink,
    }).jPlayer("play");
}

function updateUI(){
    $.getJSON('/pianobar/status/' + escape(currentRecordedStation),function(data){
        if(jsonCompare(preivousStatusData ,data)){
          $("#currentSongTitle").text(data.currentSong.title);
          $("#currentSongAlbum").text(data.currentSong.album);
          $("#currentSongArtist").text(data.currentSong.artist);
          $('#currentSongCover').text("").append($('<img>')
                                         .attr('src','/Pandora/' + escape( 
                                                data.currentSong.artist + '---' +
                                                data.currentSong.album + '---' +
                                                data.currentSong.title + '.jpg'))
                                         .attr('class','songCoverImg')
                                         .attr('title', data.currentSong.title + ' - ' +
                                                        data.currentSong.album));
          
          $('#RealTimeChannelSelector').find('option').remove();
          $('#ReplayChannelSelector').find('option').remove();
          $('#ReplayChannelSelector').append($('<option>')
                                            .text("All")
                                            .attr('value', "All"));          
          for(var stationIndex in data.stations) {
            var station = data.stations[stationIndex];
            $('#RealTimeChannelSelector').append($('<option>')
                                                    .text(station.title)
                                                    .attr('value', station.id)
                                                    .attr('selected',station.selected));
            var currentSelectedRStation = false;
            if(currentRecordedStation === station.title){
                currentSelectedRStation = true;
            }
            $('#ReplayChannelSelector').append($('<option>')
                                                .text(station.title)
                                                .attr('value', station.id)
                                                .attr('selected',currentSelectedRStation));
          }
          $('#recordList').text("");
          for(var songIndex in data.recordedSongs) {
            var song = data.recordedSongs[songIndex];
            $('#recordList').append($('<li>').append($('<img>')
                            .attr('src','/Pandora/' + escape(currentRecordedStation) + '/' +
                                 escape(song.artist + '---' + song.album + '---' + song.title + '.jpg'))
                            .attr('title', song.title + " - " + song.album)
                            .attr('class', 'recordedSongCover')
                            .click(playRecord)));
          }          
          preivousStatusData = data;
        }
    });    
}
$(document).ready(function() {
    $("#RealTimeChannelSelector").change(function () {
          $("#RealTimeChannelSelector option:selected").each(function () {
            $("#jquery_jplayer").jPlayer("clearMedia");
            currentRecordedStation = $(this).text();
            $.getJSON('/pianobar/action/selectStation/'+$(this).attr('value'),function(data){  
            });
          });
    }).trigger('change');
    
    $("#ReplayChannelSelector").change(function () {
          $("#ReplayChannelSelector option:selected").each(function () {
                currentRecordedStation = $(this).text();
                updateUI();
          });
    }).trigger('change');
    
    $("#Play").click(function () {
        $("#jquery_jplayer").jPlayer("clearMedia");
        $.getJSON('/pianobar/action/playPause',function(data){  
        });
    });
    $("#Next").click(function () {
        $("#jquery_jplayer").jPlayer("clearMedia");
        $.getJSON('/pianobar/action/next',function(data){  
        });
    });
    $("#Stop").click(function () {
        $.getJSON('/pianobar/action/stop',function(data){  
        });
    });
    $("#Like").click(function () {
        $.getJSON('/pianobar/action/like',function(data){  
        });
    });
    $("#UnLike").click(function () {
        $.getJSON('/pianobar/action/unlike',function(data){  
        });
    });
    $("#jquery_jplayer").jPlayer({
      swfPath: "/javascripts/jPlayer/Jplayer.swf",
      supplied: "m4a",
      solution: "flash, html",
    });
    updateUI();
    $("#currentSong").show();
    window.setInterval(updateUI, 15000);
});